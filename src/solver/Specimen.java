package solver;

import graph.Graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Basic unit of the evolutionary algorithm. It stores information
 * about the solution it is related to and has methods allowing producing other
 * Specimens. Mutation is implemented as 2opt with limited number of iterations and
 * random edge swap. Edge recombination is used as crossover operator.
 */
public class Specimen {
    private final int[] chromosome;
    private final Graph graph;
    private double distance;

    /**
     * Constructor creating random Specimen for a given graph.
     * @param graph graph related to this Specimen
     */
    Specimen(Graph graph) {
        this.graph = graph;
        chromosome = new int[graph.getSize()];
        initChromosome();
        shuffleChromosome();
        calcDistance();
    }

    /**
     * Generates offspring of this Specimen and another one. Mutation parameters are
     * also specified.
     * @param parent the second parent of a new Specimen
     * @param iterations max number of 2opt iterations in mutation
     * @param randMutate chance of random vertex swap in mutation
     * @return new Specimen being an offspring of a given pair
     */
    public Specimen generateOffspring(Specimen parent, int iterations, int randMutate) {
        Specimen child = new Specimen(this,parent);
        child.mutate(iterations, randMutate);
        return child;
    }

    /**
     * Returns the length of a route represented by this Specimen.
     * @return length of a route
     */
    public double getDistance() {
        return distance;
    }

    /**
     * Returns the route represented by this Specimen. The route is represented as
     * a sequence of vertices' indices.
     * @return route represented by this Specimen
     */
    public int[] getChromosome() {
        return chromosome.clone();
    }

    private void calcDistance() {
        distance = 0;
        for (int i = 0; i < chromosome.length - 1; ++i)
            distance += graph.getDistance(chromosome[i],chromosome[i+1]);
        distance += graph.getDistance(chromosome[chromosome.length-1],chromosome[0]);
    }

    private void shuffleChromosome() {
        Random generator = new Random();
        for (int i = 1; i < chromosome.length; ++i) {
            int swapIndex = generator.nextInt(chromosome.length - i) + i;
            int temp = chromosome[i];
            chromosome[i] = chromosome[swapIndex];
            chromosome[swapIndex] = temp;
        }
    }

    private void initChromosome() {
        for (int i = 0; i < chromosome.length; ++i)
            chromosome[i] = i;
    }

    private double swappedDistance(int i, int j) {
        double newDistance = distance;
        int indexBefore = (chromosome.length+i-1)%chromosome.length;
        int indexAfter = (j+1)%chromosome.length;

        newDistance -= graph.getDistance(chromosome[indexBefore],chromosome[i]);
        newDistance -= graph.getDistance(chromosome[j],chromosome[indexAfter]);
        newDistance += graph.getDistance(chromosome[indexBefore],chromosome[j]);
        newDistance += graph.getDistance(chromosome[i],chromosome[indexAfter]);

        return newDistance;
    }

    private void swap2Opt(int i, int j) {
        int middle = (i+j)/2;

        for (; i <= middle; ++i) {
            int temp = chromosome[i];
            chromosome[i] = chromosome[j];
            chromosome[j] = temp;
            --j;
        }
    }

    private void mutate(int maxIterations, int randMutate) {
        mutateRand(randMutate);

        Random generator = new Random();
        int start = generator.nextInt(chromosome.length - 1) + 1;
        int iter = 0;
        while (iter < maxIterations) {
            int swapped = mutate2opt(start,chromosome.length,maxIterations-iter);
            swapped += mutate2opt(1,start,maxIterations-iter-swapped);
            if (swapped == 0)
                return;
            iter += swapped;
        }
    }

    private int mutate2opt(int start, int end, int maxIter) {
        if (maxIter == 0)
            return 0;
        int result = 0;
        swapStart:
        for (int i = start; i < end; ++i) {
            for (int j = i + 1; j < chromosome.length; ++j) {
                double newDistance = swappedDistance(i, j);
                if (newDistance < distance) {
                    swap2Opt(i, j);
                    distance = newDistance;
                    ++result;
                    if (result >= maxIter)
                        break swapStart;
                }
            }
        }
        return result;
    }

    private void mutateRand(int randMutate) {
        Random generator = new Random();

        if (generator.nextInt(100) < randMutate) {
            int chosenIndex = generator.nextInt(chromosome.length-1)+1;
            int destinationIndex = generator.nextInt(chromosome.length-1)+1;

            int chosen = chromosome[chosenIndex];
            if (chosenIndex <= destinationIndex)
                for (int i = chosenIndex; i < destinationIndex; ++i)
                    chromosome[i] = chromosome[i+1];
            else
                for (int i = chosenIndex; i > destinationIndex; --i)
                    chromosome[i] = chromosome[i-1];
            chromosome[destinationIndex] = chosen;
            calcDistance();
        }
    }

    private Specimen(Specimen father, Specimen mother) {
        chromosome = new int[father.chromosome.length];
        graph = father.graph;
        EdgeMap edgeMap = new EdgeMap(father,mother);

        for (int i = 0; i < chromosome.length; ++i)
            chromosome[i] = edgeMap.getNext();
        calcDistance();
    }

    private class EdgeMap {
        private final ArrayList<HashSet<Integer>> list;
        private int next;

        EdgeMap(Specimen father, Specimen mother) {
            list = new ArrayList<>();
            int length = father.chromosome.length;
            for (int i = 0; i < length; ++i)
                list.add(i, new HashSet<>());

            for (int i = 0; i < length; ++i) {
                addNeighbours(i,father);
                addNeighbours(i,mother);
            }
            next = 0;
        }

        int getNext() {
            ArrayList<Integer> candidates = findCandidates();
            list.set(next,null);
            int result = next;
            updateNext(candidates);
            return result;
        }

        private void addNeighbours(int i, Specimen parent) {
            int length = parent.chromosome.length;
            int leftNeighbour = parent.chromosome[(length+i-1)%length];
            int rightNeighbour = parent.chromosome[(i+1)%length];

            list.get(parent.chromosome[i]).add(leftNeighbour);
            list.get(parent.chromosome[i]).add(rightNeighbour);
        }

        private ArrayList<Integer> findCandidates() {
            ArrayList<Integer> candidates = new ArrayList<>();
            int neighbours = 5; //max is 4

            for (int i : list.get(next)) {
                HashSet<Integer> set = list.get(i);
                set.remove(next);
                if (set.size() < neighbours) {
                    neighbours = set.size();
                    candidates.clear();
                }
                if (set.size() <= neighbours)
                    candidates.add(i);
            }
            return candidates;
        }

        private void updateNext(ArrayList<Integer> candidates) {
            if (candidates.isEmpty()) {
                for (int i = 0; i < list.size(); ++i) {
                    if (list.get(i) != null) {
                        next = i;
                        break;
                    }
                }
            } else {
                Random generator = new Random();
                int chosen = generator.nextInt(candidates.size());
                next = candidates.get(chosen);
            }
        }
    }
}
