package solver;

/**
 * Container for solver settings. It is used only for Solver initialization.
 */
public class SolverSettings {
    /**
     * Number of Specimens in each generation.
     */
    public final int mi;
    /**
     * Number of new Specimens created in each generation.
     */
    public final int lambda;
    /**
     * Max number of 2opt iterations in every mutation.
     */
    public final int optIter;
    /**
     * Max number of generations.
     */
    public final int maxIter;
    /**
     * Number of generations without improvement after which calculations are stopped.
     */
    public final int maxNoBetter;
    /**
     * Chance of random mutation in percents.
     */
    public final int randMutate;

    /**
     * Creates SolverSettings object with given fields' values.
     * @param mi number of Specimens in each generation
     * @param lambda number of new Specimens created in each generation
     * @param optIter max number of 2opt iterations in every mutation
     * @param maxIter max number of generations
     * @param maxNoBetter number of generations without improvement after which calculations are stopped
     * @param randMutate chance of random mutation in percents
     */
    public SolverSettings(int mi, int lambda, int optIter, int maxIter, int maxNoBetter, int randMutate) {
        this.mi = mi;
        this.lambda = lambda;
        this.optIter = optIter;
        this.maxIter = maxIter;
        this.maxNoBetter = maxNoBetter;
        this.randMutate = randMutate;
    }
}
