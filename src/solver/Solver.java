package solver;

import app.ProgressNotifier;
import graph.Graph;

import java.util.ArrayList;

/**
 * Class managing evolutionary algorithm flow.
 */
public class Solver {
    private SolverSettings settings;
    private final ProgressNotifier progressNotifier;

    /**
     * Creates Solver connected with specified ProgressNotifier.
     * @param progressNotifier object managing indication of Solver's progress
     */
    public Solver(ProgressNotifier progressNotifier) {
        this.progressNotifier = progressNotifier;
    }

    /**
     * Sets settings of this Solver.
     * @param settings given Solver settings
     */
    public void initSolver(SolverSettings settings) {
        this.settings = settings;
    }

    /**
     * Starts the algorithm for a given graph. Settings of solution are
     * specified by initSolver() method.
     * @param graph graph for which solution is generated
     * @return list of best Specimens in every generation
     */
    public ArrayList<Specimen> solve(Graph graph) {
        ArrayList<Specimen> best = new ArrayList<>();
        Population population = new Population
                (graph,settings.mi,settings.lambda,settings.optIter,settings.randMutate);

        best.add(population.getBestSpecimen());

        boolean found = false;
        int noBetter = 0;
        for (int i = 0; i < settings.maxIter; ++i) {
            if (Thread.interrupted())
                return best;
            progressNotifier.updateProgress(i/(double)settings.maxIter);

            population.nextGeneration();
            best.add(population.getBestSpecimen());
            noBetter = calcNoBetter(noBetter,best);

            if (noBetter >= settings.maxNoBetter) {
                found = true;
                break;
            }
        }
        if (found)
            clearBest(best);

        return best;
    }

    private int calcNoBetter(int noBetter, ArrayList<Specimen> best) {
        double eps = 1e-6;
        if (best.size() > 1) {
            Specimen currentBest = best.get(best.size()-1);
            Specimen lastBest = best.get(best.size()-2);

            if (currentBest.getDistance() >= lastBest.getDistance() ||
                    lastBest.getDistance() - currentBest.getDistance() < eps)
                ++noBetter;
            else
                noBetter = 0;
        }
        return noBetter;
    }

    private void clearBest(ArrayList<Specimen> best) {
        int size = best.size();
        for (int i = size - 1; i > size - settings.maxNoBetter; --i)
            best.remove(i);
    }
}
