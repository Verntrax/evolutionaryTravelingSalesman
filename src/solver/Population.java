package solver;

import graph.Graph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

/**
 * Population of Specimens of the evolutionary algorithm. It manages crossover
 * and selection of among generations according to specified parameters.
 */
public class Population {
    private ArrayList<Specimen> specimens;

    private final int mi;
    private final int lambda;
    private final int iter;
    private final int randMutate;

    /**
     * Constructor creating Population with specified parameters.
     * @param graph graph related to Specimens in this Population
     * @param mi number of Specimens in each generation
     * @param lambda number of new Specimens created in each generation
     * @param iter max number of 2opt iterations in every mutation
     * @param randMutate chance of random mutation in percents
     */
    Population(Graph graph, int mi, int lambda, int iter, int randMutate) {
        this.mi = mi;
        this.lambda = lambda;
        this.iter = iter;
        this.randMutate = randMutate;

        specimens = new ArrayList<>();

        for (int i = 0; i < mi; ++i)
            specimens.add(new Specimen(graph));
        sortSpecimens();
    }

    /**
     * Performs one step of an algorithm. It produces new Specimens and
     * manages their selection.
     */
    public void nextGeneration() {
        specimens = chooseSpecimens(lambda);
        generateOffspring(lambda,lambda);
        sortSpecimens();
        specimens = chooseSpecimens(mi);
        sortSpecimens();
    }

    /**
     * Returns best Specimen in current generation.
     * @return best Specimen in current generation
     */
    public Specimen getBestSpecimen() {
        return specimens.get(specimens.size()-1);
    }

    private void sortSpecimens() {
        Comparator<Specimen> comparator = (s1, s2) -> {
            double result = s1.getDistance() - s2.getDistance();
            if (result > 0)
                return -1;
            if (result < 0)
                return 1;
            return 0;
        };
        specimens.sort(comparator);
    }

    private Specimen chooseSpecimen() {
        //chosen according to the roulette wheel rule
        int randBound = specimens.size()*(specimens.size()+1)/2;
        Random generator = new Random();
        int rand = generator.nextInt(randBound);
        int chosenOne = ((int)Math.sqrt(1+8*rand)-1)/2;
        return specimens.get(chosenOne);
    }

    private void generateOffspring(int quantity, int parents) {
        Random generator = new Random();
        for (int i = 0; i < quantity; ++i) {
            Specimen father = specimens.get(i); //every Specimen has at least one child
            Specimen mother = specimens.get(generator.nextInt(parents));

            specimens.add(father.generateOffspring(mother,iter,randMutate));
        }
    }

    private ArrayList<Specimen> chooseSpecimens(int quantity) {
        ArrayList<Specimen> chosenSpecimens = new ArrayList<>();
        for (int i = 0; i < quantity; ++i)
            chosenSpecimens.add(chooseSpecimen());
        return chosenSpecimens;
    }
}
