package graph;

/**
 * Vertex of a graph. Stores info about vertex location in
 * coordinate system.
 */
public class Vertex {
    /**
     * X coordinate of this vertex.
     */
    public final int coordX;
    /**
     * Y coordinate of this vertex.
     */
    public final int coordY;

    /**
     * Creates vertex with given coordinates.
     * @param coordX X coordinate
     * @param coordY Y coordinate
     */
    Vertex(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }
}
