package graph;

import java.util.ArrayList;

/**
 * Class representing a complete graph. The graph consists of
 * vertices with specified location. Distance between them is
 * calculated according to euclidean metric.
 */
public class Graph {
    private final ArrayList<Vertex> vertices;

    /**
     * Creates an empty graph.
     */
    Graph() {
        vertices = new ArrayList<>();
    }

    /**
     * Add vertex with specified location to the graph.
     * @param coordX X coordinate of the vertex
     * @param coordY Y coordinate of the vertex
     */
    public void addVertex(int coordX, int coordY) {
        vertices.add(new Vertex(coordX,coordY));
    }

    /**
     * Returns list of all vertices in the graph.
     * @return list of vertices
     */
    public ArrayList<Vertex> getVertices() {
        return new ArrayList<>(vertices);
    }

    /**
     * Returns distance between two vertices.
     * @param i index of a first vertex
     * @param j index of a second vertex
     * @return distance between vertices
     */
    public double getDistance(int i, int j) {
        Vertex vertex1 = vertices.get(i);
        Vertex vertex2 = vertices.get(j);

        double partX = vertex1.coordX - vertex2.coordX;
        partX *= partX;
        double partY = vertex1.coordY - vertex2.coordY;
        partY *= partY;

        return Math.sqrt(partX + partY);
    }

    /**
     * Returns number of vertices.
     * @return number of vertices
     */
    public int getSize() {
        return vertices.size();
    }
}
