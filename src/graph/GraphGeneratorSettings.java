package graph;

/**
 * Container for graph generator settings. Simplifies arguments passing
 * to GraphGenerator instance.
 */
public class GraphGeneratorSettings {
    /**
     * Grid size for graph generation.
     */
    public final int size;
    /**
     * Probability of generating a vertex in each grid point in percents.
     */
    public final int probability;

    /**
     * Creates GraphGeneratorSettings object with given fields' values.
     * @param size grid size for graph generation
     * @param probability probability of generating a vertex in each grid point in percents
     */
    public GraphGeneratorSettings(int size, int probability) {
        this.size = size;
        this.probability = probability;
    }
}
