package graph;

import java.util.Random;

/**
 * Class used for graph generation. Generated graph has its vertices
 * located in nodes of a grid of the specified size.
 */
public class GraphGenerator {
    /**
     * Creates GraphGenerator.
     */
    public GraphGenerator() {}

    /**
     * Generates graph with specified settings.
     * @param settings given generator settings
     * @return new randomly generated graph
     */
    public Graph generateGraph(GraphGeneratorSettings settings) {
        Graph result = new Graph();

        Random generator = new Random();
        for (int i = 0; i < settings.size; ++i) {
            for (int j = 0; j < settings.size; ++j) {
                if (generator.nextInt(100) < settings.probability)
                    result.addVertex(i,j);
            }
        }
        return result;
    }
}
