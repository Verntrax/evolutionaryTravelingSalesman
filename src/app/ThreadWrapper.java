package app;

/**
 * Container for thread object. It simplifies inter-thread
 * communication.
 */
public class ThreadWrapper {
    /**
     * Stored thread.
     */
    public Thread thread;
}
