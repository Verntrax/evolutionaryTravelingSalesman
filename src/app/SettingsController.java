package app;

import graph.GraphGeneratorSettings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import solver.SolverSettings;

/**
 * Controller of settings window.
 */
public class SettingsController extends GridPane {
    private Stage settingsStage;

    private SolverSettings solverSettings;
    private GraphGeneratorSettings graphSettings;

    @FXML
    private ComboBox<Integer> miBox;
    @FXML
    private ComboBox<Integer> lambdaBox;
    @FXML
    private ComboBox<Integer> mutationBox;
    @FXML
    private ComboBox<Integer> optBox;
    @FXML
    private ComboBox<Integer> iterBox;
    @FXML
    private ComboBox<Integer> stopBox;
    @FXML
    private ComboBox<Integer> sizeBox;
    @FXML
    private ComboBox<Integer> probBox;

    /**
     * Creates settings window controller connected with application.
     * @param mainStage main stage of the application
     * @throws Exception any application exception
     */
    SettingsController(Stage mainStage) throws Exception {
        solverSettings = new SolverSettings(20,10,10,1000,10,0);
        graphSettings = new GraphGeneratorSettings(2,100);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("settingsWindow.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        settingsStage = new Stage();
        settingsStage.setTitle("Ustawienia");
        settingsStage.setScene(new Scene(this, 500, 206));
        settingsStage.initModality(Modality.WINDOW_MODAL);
        settingsStage.initOwner(mainStage);

        initBoxes();
    }

    /**
     * Shows settings window.
     */
    public void showWindow() {
        miBox.setValue(solverSettings.mi);
        initLambdaBox();
        lambdaBox.setValue(solverSettings.lambda);
        mutationBox.setValue(solverSettings.randMutate);
        optBox.setValue(solverSettings.optIter);
        iterBox.setValue(solverSettings.maxIter);
        stopBox.setValue(solverSettings.maxNoBetter);

        sizeBox.setValue(graphSettings.size);
        probBox.setValue(graphSettings.probability);

        settingsStage.show();
    }

    /**
     * Returns inserted GraphGenerator settings.
     * @return GraphGenerator settings
     */
    public GraphGeneratorSettings getGraphSettings() {
        return graphSettings;
    }

    /**
     * Returns inserted Solver settings.
     * @return Solver settings
     */
    public SolverSettings getSolverSettings() {
        return solverSettings;
    }


    private void initBoxes() {
        initMiBox();
        initMutationBox();
        initOptBox();
        initIterBox();
        initStopBox();

        initSizeBox();
        initProbBox();
    }

    private void initMiBox() {
        for (int i = 1; i <= 200; ++i)
            miBox.getItems().add(i*20);
    }

    private void initLambdaBox() {
        lambdaBox.getItems().clear();

        for (int i = 5; i <= 10; ++i)
            lambdaBox.getItems().add((int)(miBox.getValue()*0.1*i));
    }

    private void initMutationBox() {
        for (int i = 0; i <= 100; ++i)
            mutationBox.getItems().add(i);
    }

    private void initOptBox() {
        for (int i = 0; i <= 100; ++i)
            optBox.getItems().add(i*10);
    }

    private void initIterBox() {
        for (int i = 0; i <= 100; ++i)
            iterBox.getItems().add(i*100);
    }

    private void initStopBox() {
        for (int i = 1; i <= 100; ++i)
            stopBox.getItems().add(i*10);
    }

    private void initSizeBox() {
        for (int i = 2; i <= 40; ++i)
            sizeBox.getItems().add(i);
    }

    private void initProbBox() {
     for (int i = 1; i <= 10; ++i)
            probBox.getItems().add(i*10);
    }

    /**
     * Updates lambda box values. They can change due to change
     * in chosen mi parameter value.
     */
    @FXML
    public void updateLambdaBox() {
        initLambdaBox();
        lambdaBox.setValue(miBox.getValue()/2);
    }

    /**
     * Accepts changes in settings and closes settings window.
     */
    @FXML
    public void approveChanges() {
        solverSettings = new SolverSettings(
                miBox.getValue(),
                lambdaBox.getValue(),
                optBox.getValue(),
                iterBox.getValue(),
                stopBox.getValue(),
                mutationBox.getValue());

        graphSettings = new GraphGeneratorSettings(
                sizeBox.getValue(),
                probBox.getValue());

        settingsStage.close();
    }

    /**
     * Closes settings window.
     */
    @FXML
    public void discardChanges() {
        settingsStage.close();
    }
}
