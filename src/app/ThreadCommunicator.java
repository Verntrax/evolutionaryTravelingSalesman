package app;

/**
 * Class supporting communication between solver and gui thread.
 * All class methods are thread-safe. Two pieces of information
 * are exchanged via this class' object: progress of calculations
 * and solver flag (done/not done).
 */
public class ThreadCommunicator {
    private boolean solverDone;
    private double progress;

    /**
     * Creates ThreadCommunicator object. solverDone value is
     * set to true.
     */
    ThreadCommunicator() {
        solverDone = true;
    }

    /**
     * Sets solverDone value.
     * @param solverDone given value
     */
    synchronized public void setSolverDone(boolean solverDone) {
        this.solverDone = solverDone;
    }

    /**
     * Returns solverDone value.
     * @return solverDone value
     */
    synchronized public boolean getSolverDone() {
        return solverDone;
    }

    /**
     * Sets progress value.
     * @param progress given value
     */
    synchronized public void setProgress(double progress) {
        this.progress = progress;
    }

    /**
     * Returns progress value.
     * @return progress value
     */
    synchronized public double getProgress() {
        return progress;
    }
}
