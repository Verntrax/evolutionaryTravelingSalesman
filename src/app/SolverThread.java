package app;

import graph.Graph;
import javafx.application.Platform;
import solver.Solver;
import solver.Specimen;

import java.util.ArrayList;

/**
 * Class of solver thread. It is responsible for solver initialization,
 * and running calculations.
 */
public class SolverThread implements Runnable {
    private final Solver solver;
    private final ThreadCommunicator threadCommunicator;
    private final MainController mainController;
    private Graph graph;

    /**
     * List of specimens returned by Solver object - result of calculations.
     */
    public ArrayList<Specimen> solution;

    /**
     * Creates SolverThread connected with the application.
     * @param solver Solver object used for calculations
     * @param threadCommunicator object used for communication with gui thread
     * @param mainController gui main window controller
     */
    SolverThread(Solver solver, ThreadCommunicator threadCommunicator, MainController mainController) {
        this.solver = solver;
        this.threadCommunicator = threadCommunicator;
        this.mainController = mainController;
    }

    /**
     * Sets graph to be solved by Solver.
     * @param graph graph to be solved
     */
    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    /**
     * Runs this thread.
     */
    public void run() {
        solution = solver.solve(graph);
        if (Thread.interrupted())
            return;
        Platform.runLater(mainController::showResults);
        threadCommunicator.setSolverDone(true);
    }
}
