package app;

import graph.Graph;
import graph.GraphGenerator;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import solver.Solver;
import solver.Specimen;

import java.util.ArrayList;

/**
 * Main window controller.
 */
public class MainController extends GridPane {
    private final Solver solver;
    private final SolverThread solverThread;
    private final ThreadWrapper threadWrapper;
    private final ThreadCommunicator threadCommunicator;
    private ArrayList<Specimen> solution;
    private Graph graph;
    private int graphSize;
    private final GraphDrawer drawer;
    private final SettingsController settingsController;
    private final ProgressNotifier progressNotifier;

    @FXML
    private Canvas graphPicture;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Slider generationPicker;
    @FXML
    private Label resultLabel;
    @FXML
    private LineChart<String,Double> chart;

    /**
     * Creates main window controller connected with application.
     * @param mainStage main stage of the application
     * @param threadWrapper thread wrapper for solver thread
     * @throws Exception any exception thrown by application
     */
    MainController(Stage mainStage, ThreadWrapper threadWrapper) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mainWindow.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        mainStage.setTitle("Komiwojażer");
        mainStage.setScene(new Scene(this, 1844, 875));

        settingsController = new SettingsController(mainStage);
        drawer = new GraphDrawer(graphPicture, 10);
        threadCommunicator = new ThreadCommunicator();
        progressNotifier = new ProgressNotifier(this, threadCommunicator);
        solver = new Solver(progressNotifier);
        solverThread = new SolverThread(solver, threadCommunicator, this);
        this.threadWrapper = threadWrapper;

        generationPicker.valueProperty().addListener((ov, old_val, new_val) -> {
            if(threadCommunicator.getSolverDone()) {
                int chosenSolutionId = (int) new_val.doubleValue();
                int[] chosenSolution = solution.get(chosenSolutionId).getChromosome();
                drawer.drawGraph(graphSize, graph, chosenSolution);
                resultLabel.setText(((Double)solution.get(chosenSolutionId).getDistance()).toString());
            }
        });
        mainStage.show();
    }

    /**
     * Shows results of solver calculation.
     */
    public void showResults() {
        progressBar.setOpacity(0);
        progressBar.setProgress(0);
        fillGenerationPicker();
        drawChart();
    }

    /**
     * Updates progress bar. Progress value is set by solver thread.
     */
    public void updateProgress() {
        progressBar.setProgress(threadCommunicator.getProgress());
    }

    private void showGraph() {
        drawer.drawGraph(settingsController.getGraphSettings().size, graph, null);
    }

    private void initSolver() {
        solver.initSolver(settingsController.getSolverSettings());
    }

    private void fillGenerationPicker() {
        solution = solverThread.solution;
        generationPicker.setValue(0);
        generationPicker.setMax(solution.size()-1);
        generationPicker.setMajorTickUnit(solution.size() >= 4 ? solution.size() / 4 : 1);
        generationPicker.setDisable(false);
    }

    private void drawChart() {
        chart.getData().clear();
        XYChart.Series<String,Double> series = new XYChart.Series<>();
        series.setName("MyData");
        for (int i = 0; i < solution.size(); ++i)
            series.getData().add(new XYChart.Data<>(((Integer)i).toString(),solution.get(i).getDistance()));
        chart.getData().add(series);
    }

    private void clearResults() {
        chart.getData().clear();
        generationPicker.setDisable(true);
        generationPicker.setMax(0);
        generationPicker.setValue(0);
        resultLabel.setText("");
    }

    private void showGraphError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Błąd");
        alert.setHeaderText("Graf ma za mało wierzchołków!");
        alert.setContentText("Minimalna liczba to 4. " +
                "Wciśnij przycisk \"Generuj\", aby utworzyć graf.");

        alert.showAndWait();
    }

    private void runSolver() {
        initSolver();
        solverThread.setGraph(graph);
        threadWrapper.thread  = new Thread(solverThread);
        threadCommunicator.setSolverDone(false);
        threadWrapper.thread.start();
    }

    /**
     * Opens settings window.
     */
    @FXML
    public void openSettingsWindow() {
        if (threadCommunicator.getSolverDone())
            settingsController.showWindow();
    }

    /**
     * Generates graph. Generator settings are specified via settings window.
     */
    @FXML
    public void generateGraph() {
        if (threadCommunicator.getSolverDone()) {
            graphSize = settingsController.getGraphSettings().size;
            GraphGenerator generator = new GraphGenerator();
            graph = generator.generateGraph(settingsController.getGraphSettings());
            clearResults();
            showGraph();
        }
    }

    /**
     * Runs solver thread. Shows error message in case of invalid graph
     * generation.
     */
    @FXML
    public void solve() {
        if (graph == null || graph.getSize() < 4) {
            showGraphError();
        }
        else if (threadCommunicator.getSolverDone()) {
            progressBar.setOpacity(1);
            runSolver();
        }
    }
}
