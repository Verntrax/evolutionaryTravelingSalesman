package app;

import javafx.application.Platform;

/**
 * Class supporting inter-thread communication concerning progress display in gui.
 */
public class ProgressNotifier {
    private final MainController mainController;
    private final ThreadCommunicator threadCommunicator;

    /**
     * Creates ProgressNotifier connected to the application.
     * @param mainController main window controller
     * @param threadCommunicator object used for communication with gui thread
     */
    ProgressNotifier(MainController mainController, ThreadCommunicator threadCommunicator) {
        this.mainController = mainController;
        this.threadCommunicator = threadCommunicator;
    }

    /**
     * Sends order to update progress bar.
     * @param progress given progress value
     */
    public void updateProgress(double progress) {
        threadCommunicator.setProgress(progress);
        Platform.runLater(mainController::updateProgress);
    }
}
