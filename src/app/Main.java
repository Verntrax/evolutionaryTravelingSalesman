package app;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main application class.
 */
public class Main extends Application {
    private ThreadWrapper threadWrapper;

    /**
     * Starts application.
     * @param primaryStage application primary stage
     * @throws Exception any exception thrown by application
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        threadWrapper = new ThreadWrapper();
        MainController controller = new MainController(primaryStage,threadWrapper);
    }

    /**
     * Stops application. Takes care of stopping solver thread.
     */
    @Override
    public void stop() {
        if (threadWrapper.thread != null)
            threadWrapper.thread.interrupt();
    }

    /**
     * Main method.
     * @param args program arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
