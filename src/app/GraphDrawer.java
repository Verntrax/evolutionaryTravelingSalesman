package app;

import graph.Graph;
import graph.Vertex;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Class used for drawing graphs on Canvas.
 */
public class GraphDrawer {
    private final Canvas canvas;
    private final int pointSize;

    /**
     * Creates GraphDrawer connected with Canvas and with specified
     * vertex size.
     * @param canvas Canvas to draw on
     * @param pointSize radius of vertex in pixels
     */
    GraphDrawer(Canvas canvas, int pointSize) {
        this.canvas = canvas;
        this.pointSize = pointSize;
    }

    /**
     * Draws given graph on Canvas.
     * @param spaceSize grid size of a graph
     * @param graph graph to be drawn
     * @param path path in the graph to be drawn (null means no path)
     */
    public void drawGraph(int spaceSize, Graph graph, int[] path) {
        double unit = calculateUnit(spaceSize,graph);

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.clearRect(0, 0,canvas.getHeight(),canvas.getWidth());

        ArrayList<Vertex> vertices = graph.getVertices();

        for (Vertex vertex : vertices) {
            double x = unit * vertex.coordX;
            double y = unit * vertex.coordY;
            gc.fillOval(x, y, pointSize, pointSize);
        }
        if (path != null) {
            drawPath(unit,vertices,path);
        }
    }

    private void drawPath(double unit, ArrayList<Vertex> vertices, int[] path) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.setLineWidth(pointSize/3);

        for (int i = 0; i < path.length; ++i) {
            int first = path[i];
            int second = path[(i+1)%path.length];

            double firstX = vertices.get(first).coordX*unit + pointSize/2;
            double firstY = vertices.get(first).coordY*unit + pointSize/2;
            double secondX = vertices.get(second).coordX*unit + pointSize/2;
            double secondY = vertices.get(second).coordY*unit + pointSize/2;

            gc.strokeLine(firstX,firstY,secondX,secondY);
        }
    }

    private double calculateUnit(int spaceSize, Graph graph) {
        double result = canvas.getHeight();
        result -= pointSize;
        result /= spaceSize - 1;
        return result;
    }
}
